export default {
  CharacterNotFound: {
    name: 'CharacterNotFoundError',
    status: 404,
    message: 'No character found with that id'
  }
};
